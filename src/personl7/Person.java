/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personl7;

import static java.time.Clock.system;

/**
 *
 * @author Admin
 */
public class Person {
    
    private String name;
    private double height;
    private double weight;
    Weight unitWeight;
    Height unitHeight;

    Person(){}

    Person(String john, int i, double d) {
        //To change body of generated methods, choose Tools | Templates.
    }
    
    public enum Weight {
        K, LB
    }

    public enum Height {
        M, IN
    }
    
    public Person(String name, double height, double weight, Height unitHeight, Weight unitWeight) {
        this.name = name;
        this.height = height;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    public double getBMI() {
        
        double weightInKilos = 0;
        double heightInMeters = 0;
        
        switch (unitWeight ) {
            case K : weightInKilos = weight;
            
            case LB : weightInKilos = weight * 2.205;
            break;
        }
        
        switch (unitHeight) {
            case M : heightInMeters = height;
            
            case IN : heightInMeters = height * 0.0254;
            break;
        }
              
        double bmi = weightInKilos / heightInMeters;
        return bmi;
    }
}
